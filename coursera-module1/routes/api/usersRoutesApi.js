let express = require('express');
let router = express.Router();
let userController = require('../../controllers/api/usersControllerApi');

router.get('/', userController.usersList);
router.post('/create', userController.userCreate);
router.post('/reserve', userController.userReservation);

module.exports = router;