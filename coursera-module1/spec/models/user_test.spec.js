let mongoose = require('mongoose');
let Bicycle = require('../../models/bicycle');
let User = require('../../models/user')
let Reservation = require('../../models/reservation')

describe('Testing Usuario', () => {
	beforeAll((done) => { mongoose.connection.close(done) });

	beforeEach((done) => {
		mongoose.disconnect();
		let mongoDB = "mongodb://localhost/testdb";
		mongoose.connect(mongoDB, { useNewUrlParser:true, useUnifiedTopology: true  });

		const db = mongoose.connection;
		db.on('error', console.error.bind(console, 'connection error'));
		db.once('open', () => {
			console.log('We are connected to test database');
			done();
		});
	});

	afterEach((done) => {
    Reservation.deleteMany({}, (err, success) =>  {
      if (err) {
        console.log(err);
      }

      User.deleteMany({},  (err, success) => {
        if (err) {
          console.log(err);
        }

        Bicycle.deleteMany({}, (err, success) => {
          if (err) {
            console.log(err);
          }
          mongoose.connection.close(done);
        });
      });
    });
	});

	describe('User reservation', () => {
		it('when exist', () => {
			let user = new User({ name: "John Doe"})
      user.save()
      
      const bicycle = new Bicycle({
        code:1, 
        color:"Verde", 
        model: "Urbana",
        location: [-34.5,-54.1]
      });
      bicycle.save();

      const today = "2020/10/04"
      const tomorrow = "2020/10/06"

      user.reserve[bicycle._id, today, tomorrow, (err, reservation) => {
        Reservation.find({})
          .populate('bicycle')
          .populate('user')
          .exec((err, reservations) => {
            expect(reservations.length).toBe(1);
            expect(reservations[0].reservationDays()).toBe(3);
            expect(reservation[0].bicycle.code).toBe(1);
            expect(reservations[0].user.name).toBe(user.name);
          })
      }]
    });
  });	
});
