const mongoose = require('mongoose');
const Reservation = require("../models/reservation");
const Schema = mongoose.Schema;

const userSchema = new Schema({
  name: String
})

userSchema.methods.reserve = function (bikeId, startDate, endDate, cb) {
  const reserve = new Reservation({
    user: this._id,
    bikeId,
    startDate,
    endDate
  });
  reserve.save(cb);
}

module.exports = mongoose.model("User", userSchema);
