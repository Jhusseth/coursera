var Bicileta = require('../model/Bicicleta')


exports.module_list = function(req,res){
    res.render('modules/index',{mds:Bicileta.allBicis});
}

exports.module_post = function(req,res){
    var m = new Bicileta(req.body.id,req.body.color,req.body.model)
    
    m.ubication = [req.body.latitude,req.body.longitude]

    Bicileta.addModule(m)

    res.redirect ('/modules');
}

exports.module_get_edit = function(req,res){
    var md = Bicileta.findById(req.params.id);
    res.render('modules/edit',{md});
}
exports.module_post_edit = function(req,res){

    var bici = Bicileta.findById(req.params.id);

    bici.id = req.body.id;
    bici.color = req.body.color;
    bici.model = req.body.model;
    bici.ubication = [req.body.latitude,req.body.longitude]

    res.redirect ('/modules');
}

exports.module_delete = function(req,res){
    Bicileta.removeById(req.body.id)
    res.redirect('/modules');
}