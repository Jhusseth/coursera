var express = require('express')
var router =  express.Router()
var moduleController  = require('../controllers/moduleController')

router.get("/",moduleController.module_list)
router.post("/add",moduleController.module_post)
router.post('/:id/edit',moduleController.module_post_edit)
router.get('/:id/edit',moduleController.module_get_edit)
router.post("/:id/delete",moduleController.module_delete)

module.exports = router;