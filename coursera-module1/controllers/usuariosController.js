var Usuario = require('../model/Usuario')


exports.users_list = function(req,res){
    res.render('users/index',{usrs:Usuario.allUsers});
}

exports.module_post = function(req,res){
    var m = new Usuario(req.body.name,req.body.email,req.body.password)

    Usuario.addUser(m)

    res.redirect ('/users');
}

exports.user_get_edit = function(req,res){
    var usr = Usuario.findById(req.params.id);
    res.render('users/edit',{usr});
}
exports.user_post_edit = function(req,res){

    var user = Usuario.findById(req.params.id);

    user.name = req.body.name;
    user.email = req.body.email;
    user.password = req.body.password;

    res.redirect ('/users');
}

exports.user_delete = function(req,res){
    Bicileta.removeById(req.body.id)
    res.redirect('/users');
}