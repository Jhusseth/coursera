var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bicicletaSchema = new Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number], index: { type: '2dsphere', sparse: true}
    }
});

bicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion){
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
};

bicicletaSchema.methods.toString = function() {
    return 'code: ' + this.code + ' | color: ' + this.color + ' | modelo: ' + this.modelo; 
};


bicicletaSchema.statics.allBicis = function(cb){
    return this.find({}, cb);
};

bicicletaSchema.statics.add = function(aBici, cb){
    this.create(aBici, cb)
};

bicicletaSchema.statics.findByCode = function(aCode, cb){
    return this.findOne({code: aCode}, cb);
};

bicicletaSchema.statics.removeByCode = function(aCode, cb) {
    return this.deleteOne({code: aCode}, cb);
};


module.exports = mongoose.model('Bicicleta', bicicletaSchema);



// var Bicicleta = function (id, color, modelo, ubicacion){
//     this.id = id;
//     this.color = color;
//     this.modelo = modelo;
//     this.ubicacion = ubicacion;
// }

// Bicicleta.prototype.toString = function (){
//     return 'id: ' + this.id + "// color: " + this.color;
// }

// Bicicleta.allBicis = [];
// Bicicleta.add = function(aBici){
//     Bicicleta.allBicis.push(aBici);
// }

// Bicicleta.findById = function(aBiciId){
//     var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
//     if (aBici)
//         return aBici;
//     else
//         throw new Error(`No existe una bicicleta con el ID ${aBiciId}`);
// }

// Bicicleta.removeById = function(aBiciId){
//     for (var i = 0; i < Bicicleta.allBicis.length; i++){
//         if (Bicicleta.allBicis[i].id == aBiciId){
//             Bicicleta.allBicis.splice(i, 1);
//             break;
//         }
//     }
// }

// // var a = new Bicicleta(1, 'azul', '1/2 carrera', [-34.6049932,-58.4367122,21]);
// // var b = new Bicicleta(2, "rojo", "Playera", [-34.5809316,-58.4204263,21]);
// // var c = new Bicicleta(3, "negro", "Montaña", [-34.5930425,-58.419088]);
// // var d = new Bicicleta(4, "amarillo", "Urbana", [-34.5889003,-58.4301107]);

// // Bicicleta.add(a);
// // Bicicleta.add(b);
// // Bicicleta.add(c);
// // Bicicleta.add(d);
// module.exports = Bicicleta;