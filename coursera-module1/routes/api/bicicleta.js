var express = require('express')
var router =  express.Router()
var moduleControllerAPI = require('../../controllers/api/moduleControllerAPI')

router.get('/',moduleControllerAPI.module_list)
router.post('/add',moduleControllerAPI.module_post)
router.delete('/delete',moduleControllerAPI.module_delete)

module.exports = router;