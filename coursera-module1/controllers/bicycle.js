let Bicycle = require('../models/bicycle');

exports.bicyclesList = (req, res) => {
  res.render('bicycles/index', { bicycles: Bicycle.allBicycles });
}

exports.bicycleCreateGet = (req, res) => {
  res.render('bicycles/create');
}

exports.bicycleCreatePost = (req, res) => {
  let bicycle = new Bicycle(req.body.id, req.body.color, [req.body.lat, req.body.lng], req.body.model);
  Bicycle.add(bicycle);

  res.redirect('/bicycles');
}

exports.bicycleUpdateGet = (req, res) => {
  let bicycle = Bicycle.findById(req.params.id);
  res.render('bicycles/update', {bicycle});
}

exports.bicycleUpdatePost = (req, res) => {
  let bicycle = Bicycle.findById(req.params.id);
  bicycle.id = req.body.id;
  bicycle.color = req.body.color;
  bicycle.model = req.body.model;
  bicycle.location = [req.body.lat, req.body.lng];

  res.redirect('/bicycles');
}


exports.bicycleDeletePost = (req, res) => {
  Bicycle.removeById(req.body.id);

  res.redirect('/bicycles')
}

